**Abstract**

Debido al clima subtropical y a factores sociodemográficos, la incidencia de los vectores que transmiten enfermedades tales como el dengue, zika, y la chikungunya, es muy alta en nuestro paı́s. El SENEPA, que es la institución encargada de la vigilancia y control de las enfermedades transmitidas por vectores, mediante la educación pública y los diversos programas de rastrillaje y minga busca prevenir y combatir la formación y proliferación de los criaderos de vectores. Se hace importante establecer la tipificación y distribución de estos criaderos, de manera a determinar la exposición de la población a las enfermedades transmitidas por vectores como el Aedes aegypti, en los distintos barrios y ciudades.

Con ayuda de herramientas como los Sistemas de Información Geográfica, aplicaciones móviles, y el acceso móvil a Internet, se podrı́a realizar el proceso de monitoreo, estudio, y análisis de datos de criaderos de vectores, en especial el Aedes aegypti, de una forma más eficiente. De esta manera, las estimaciones, asignaciones de recursos, y planificacion de acciones a tomar, se harı́a en base a la información recabada en campo, además de la información estimada y calculada con ayuda de los SIG.

En este trabajo se describen los procesos de recolección, visualización, y análisis aplicados en la actualidad, y se presenta una alternativa con computación móvil, sistemas de información geográfica, y modelos matemáticos basados en los teoremas de bayes, y de probabilidad total.

**Palabras clave:** Criaderos, Aedes aegypti, SENEPA, Aplicaciones móviles, SIG.